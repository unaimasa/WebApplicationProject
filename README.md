Web Application Project

Web Application Project is an app that generates a Kiosk Mode of a web page.
Kiosk Mode is useful if you develop an app and you want prevent any other applications to run in the foreground.

In this app you add an URL in the settings and once you press kiosk mode, that web page is 
going to be the only thing available on the device this app is installed on.

Is possible to remove the Kiosk Mode or change the url pressing a sequence in the web view screen.

By default, the service that starts the kiosk mode is commented.

