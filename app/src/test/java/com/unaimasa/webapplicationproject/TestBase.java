package com.unaimasa.webapplicationproject;

import com.google.android.gms.common.ConnectionResult;

import org.junit.After;
import org.junit.Before;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.Shadows;
import org.robolectric.shadows.ShadowApplication;
import org.robolectric.shadows.ShadowLog;
import org.robolectric.shadows.gms.ShadowGooglePlayServicesUtil;
import org.robolectric.util.ReflectionHelpers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Executors;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.android.LogcatAppender;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.core.FileAppender;

/**
 * Created by unai.masa on 19/01/2016.
 */
public class TestBase {
    private static final Logger logger = LoggerFactory.getLogger(TestBase.class);
    private static final String LOGCAT_LOGGER_NAME = "LOGCAT";
    private static final String LOGCAT_TAG_ENCODER_PATTERN = "%logger{0}";
    private static final String LOGCAT_MESSAGE_ENCODER_PATTERN = "[%thread] - %msg%n";
    private static final String LOG_FILE_PATH = "./test_result.log";
    private static final String FILE_APPENDER_LOGGER_NAME = "TESTS_LOGS";
    private static final String FILE_APPENDER_MESSAGE_ENCODER_PATTERN = "%d{HH:mm:ss.SSS} [%thread] %-5level %logger{35} - %msg%n";
    private static final String TAG = "Config";

    static {
        configureLogbackDirectly();
        ShadowLog.stream = System.out;
        System.setProperty("robolectric.offline", "true");
        System.setProperty("robolectric.logging.enable", "true");
    }

    @Before
    public void setUp() throws Exception {
        final ShadowApplication shadowApplication = Shadows.shadowOf(RuntimeEnvironment.application);
        shadowApplication.declareActionUnbindable("com.google.android.gms.analytics.service.START");
        ShadowGooglePlayServicesUtil.setIsGooglePlayServicesAvailable(ConnectionResult.SUCCESS);
        ReflectionHelpers.setStaticField(BuildConfig.class, "DEBUG", false);
    }

    @After
    public void tearDown() throws Exception {

    }

    @SuppressWarnings("unchecked")
    private static void configureLogbackDirectly() {
        // reset the default context (which may already have been initialized)
        // since we want to reconfigure it
        LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
        lc.reset();

        // setup LogcatAppender
        LogcatAppender logcatAppender = new LogcatAppender();
        logcatAppender.setContext(lc);
        logcatAppender.setName(LOGCAT_LOGGER_NAME);

        PatternLayoutEncoder tagEncoder = new PatternLayoutEncoder();
        tagEncoder.setContext(lc);
        tagEncoder.setPattern(LOGCAT_TAG_ENCODER_PATTERN);
        tagEncoder.start();

        PatternLayoutEncoder encoder = new PatternLayoutEncoder();
        encoder.setContext(lc);
        encoder.setPattern(LOGCAT_MESSAGE_ENCODER_PATTERN);
        encoder.start();

        logcatAppender.setTagEncoder(tagEncoder);
        logcatAppender.setEncoder(encoder);
        logcatAppender.start();

        // setup RollingFileAppender
        FileAppender fileAppender = new FileAppender();
        fileAppender.setName(FILE_APPENDER_LOGGER_NAME);
        fileAppender.setContext(lc);
        fileAppender.setFile(LOG_FILE_PATH); //    global/android/update/test_result.log

        PatternLayoutEncoder fileEncoder = new PatternLayoutEncoder();
        fileEncoder.setContext(lc);
        fileEncoder.setPattern(FILE_APPENDER_MESSAGE_ENCODER_PATTERN);
        fileEncoder.start();

        fileAppender.setEncoder(fileEncoder);
        fileAppender.start();

        // add the newly created appenders to the root logger;
        // qualify Logger to disambiguate from org.slf4j.Logger
        ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        root.setLevel(Level.TRACE);
        root.addAppender(logcatAppender);
        root.addAppender(fileAppender);

    }

    protected void logTestName(String testName) {
        log("RUNNING TEST " + testName);
    }


    protected void log(String message) {
        logger.warn("\n\n\n\n\n" +
                "\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::" +
                "\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::" +
                "\n:::::::::::::::::::::: " + message +
                "\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::" +
                "\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::\n\n\n\n\n");
    }

    public void run(Runnable r) {
        Executors.newSingleThreadExecutor().execute(r);
    }
}
