package com.unaimasa.webapplicationproject.util;

import com.unaimasa.webapplicationproject.BuildConfig;
import com.unaimasa.webapplicationproject.R;
import com.unaimasa.webapplicationproject.WebApp;
import com.unaimasa.webapplicationproject.WebApplicationProjectTestRunner;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowToast;

/**
 * Created by unai.masa on 07/12/2015.
 */
@RunWith(WebApplicationProjectTestRunner.class)
@Config(constants = BuildConfig.class)
public class TestCommonUtils extends CommonUtils {

    public static final String TEST_STRING = "test string";

    @Test
    public void testShouldShowToast(){
        CommonUtils.showToast(TEST_STRING);
        Assert.assertEquals("test string", ShadowToast.getTextOfLatestToast());
    }

    @Test
    public void testShouldShowToastFromString(){
        String expected = WebApp.getInstance().getString(R.string.app_name);
        CommonUtils.showToast(R.string.app_name);
        Assert.assertEquals(expected, ShadowToast.getTextOfLatestToast());
    }

}
