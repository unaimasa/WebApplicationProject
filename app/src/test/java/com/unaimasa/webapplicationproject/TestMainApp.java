package com.unaimasa.webapplicationproject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.annotation.Config;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNull.notNullValue;

/**
 * Created by unai.masa on 19/01/2016.
 */
@RunWith(WebApplicationProjectTestRunner.class)
@Config(constants = BuildConfig.class)
public class TestMainApp {
    @Test
    public void testGetInstance() {
        assertThat(WebApp.getInstance(), notNullValue());
    }
}
