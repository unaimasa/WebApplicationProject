package com.unaimasa.webapplicationproject;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.PowerManager;
import android.view.WindowManager;

import com.unaimasa.webapplicationproject.section.web.service.KioskService;
import com.unaimasa.webapplicationproject.section.web.receiver.OnScreenOffReceiver;
import com.unaimasa.webapplicationproject.util.SharedPreferencesUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

/**
 * Created by unai.masa on 13/01/2016.
 * General context instance.
 */
public class WebApp extends Application {

    private static final Logger mLogger = LoggerFactory.getLogger(WebApp.class);

    private static WebApp mInstance;

    private OnScreenOffReceiver onScreenOffReceiver;
    private PowerManager.WakeLock wakeLock;

    @Override
    public void onCreate() {
        super.onCreate();
        mLogger.info("Application created - {}", new Date().getTime());
        mInstance = this;
        registerKioskModeScreenOffReceiver();
        // startKioskService();
    }

    public static Context getInstance() {
        return mInstance.getApplicationContext();
    }

    public static WebApp get() {
        return mInstance;
    }

    // Register receiver for Action Screen Off
    private void registerKioskModeScreenOffReceiver() {
        // register screen off receiver
        final IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_OFF);
        onScreenOffReceiver = new OnScreenOffReceiver();
        registerReceiver(onScreenOffReceiver, filter);
    }

    // Method to get the screen always awake
    public PowerManager.WakeLock getWakeLock() {
        if(wakeLock == null) {
            // lazy loading: first call, create wakeLock via PowerManager.
            PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
            wakeLock = pm.newWakeLock(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON | PowerManager.ACQUIRE_CAUSES_WAKEUP, "wakeup");
        }
        return wakeLock;
    }

    // Start Kiosk Service
    private void startKioskService() {
        SharedPreferencesUtil.getInstance().putBool(Constants.SharedKeys.KIOSK_MODE, true);
        startService(new Intent(this, KioskService.class));
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

}
