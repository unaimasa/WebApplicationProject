package com.unaimasa.webapplicationproject.util;

import android.support.annotation.StringRes;
import android.widget.Toast;

import com.unaimasa.webapplicationproject.WebApp;
import com.unaimasa.webapplicationproject.base.ui.view.SingleToast;

import java.util.Collection;

/**
 * Created by unai.masa on 13/01/2016.
 */
public class CommonUtils {

    public static void showToast(String text, int duration) {
        SingleToast.show(text, duration);
    }

    public static void showToast(@StringRes int resId, int duration) {
        SingleToast.show(WebApp.getInstance().getText(resId), duration);
    }

    public static void showToast(String text) {
        showToast(text, Toast.LENGTH_SHORT);
    }

    public static void showToast(int resId) {
        showToast(resId, Toast.LENGTH_SHORT);
    }

    public static boolean isEmpty(Collection<?> collection) {
        boolean result = false;
        if (collection == null || collection.isEmpty()) {
            result = true;
        }
        return result;
    }
}
