package com.unaimasa.webapplicationproject.util;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.unaimasa.webapplicationproject.WebApp;

/**
 * Created by unai.masa on 14/01/2016.
 */
public class UiUtil {

    // Keyboard Methods
    public static void hideSoftKeyboard(Context context, View view) {
        if (view != null) {
            InputMethodManager inputMethodManager =
                    (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void showSoftKeyboardForView(Context context, View view) {
        if (view != null) {
            InputMethodManager inputMethodManager =
                    (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.showSoftInput(view, InputMethodManager.SHOW_FORCED);
        }
    }

    // Device Dimension in Dp Methods
    public static float getDeviceWidthInDp() {
        return (getDeviceWidthInPx() / getDisplayDensity());
    }

    public static float getDeviceHeightInDp() {
        return (getDeviceHeightInPx() / getDisplayDensity());
    }

    // Device Dimension in Px Methods
    public static float getDeviceWidthInPx() {
        return (getDisplayMetrics().widthPixels);
    }

    public static float getDeviceHeightInPx() {
        return (getDisplayMetrics().heightPixels);
    }

    // Display Methods
    public static DisplayMetrics getDisplayMetrics() {
        return WebApp.getInstance().getResources().getDisplayMetrics();
    }

    public static float getDisplayDensity() {
        return WebApp.getInstance().getResources().getDisplayMetrics().density;
    }

    // Convert Dp to Px Method
    public static int convertDpToPx(float dp) {
        return (int) (dp * getDisplayDensity());
    }
}
