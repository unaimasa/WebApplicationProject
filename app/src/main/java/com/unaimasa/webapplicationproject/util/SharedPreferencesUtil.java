package com.unaimasa.webapplicationproject.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.unaimasa.webapplicationproject.WebApp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by unai.masa on 14/01/2016.
 * Base class for working with SharedPreferences
 */
public class SharedPreferencesUtil {

    private static final Logger mLogger = LoggerFactory.getLogger(SharedPreferencesUtil.class);

    public static final String PREFERENCES_NAME = "WebAppSP";

    private static SharedPreferencesUtil sInstance = new SharedPreferencesUtil();

    private SharedPreferences mSharedPreferences;

    private SharedPreferencesUtil() {
        mSharedPreferences = WebApp.getInstance().getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    public static SharedPreferencesUtil getInstance() {
        return sInstance;
    }

    // String Methods
    public void putString(String key, String value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(key, value);
        mLogger.info("put string key {}, value {}", key, value);
        editor.apply();
    }

    public String getString(String key) {
        String value = mSharedPreferences.getString(key, "");
        mLogger.info("get string key {}, value {}", key, value);
        return value;
    }

    public String getString(String key, String defValue) {
        String value = mSharedPreferences.getString(key, defValue);
        mLogger.info("get string key {}, value {}", key, defValue);
        return value;
    }

    // Int Methods
    public void putInt(String key, int value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(key, value);
        mLogger.info("put int key {}, value {}", key, value);
        editor.apply();
    }

    public int getInt(String key) {
        int value = mSharedPreferences.getInt(key, -1);
        mLogger.info("get int key {}, value {}", key, value);
        return value;
    }

    public int getInt(String key, int defValue) {
        int value = mSharedPreferences.getInt(key, defValue);
        mLogger.info("get int key {}, value {}", key, value);
        return value;
    }

    // Float Methods
    public void putFloat(String key, float value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putFloat(key, value);
        mLogger.info("put float key {}, value {}", key, value);
        editor.apply();
    }

    public float getFloat(String key, float defaultValue) {
        float value = mSharedPreferences.getFloat(key, defaultValue);
        mLogger.info("get float key {}, value {}", key, value);
        return value;
    }

    // Boolean Methods
    public void putBool(String key, boolean value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(key, value);
        mLogger.info("put bool key {}, value {}", key, value);
        editor.apply();
    }

    public boolean getBoolean(String key) {
        boolean value = mSharedPreferences.getBoolean(key, false);
        mLogger.info("get bool key {}, value {}", key, value);
        return value;
    }

    public boolean getBoolean(String key, boolean defValue) {
        boolean value = mSharedPreferences.getBoolean(key, defValue);
        mLogger.info("get bool key {}, value {}", key, value);
        return value;
    }

    // String Set Methods
    public void putStringSet(String key, Set<String> value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putStringSet(key, value);
        mLogger.info("put string set key {}, value {}", key, value);
        editor.apply();
    }

    public Set<String> getStringSet(String key) {
        Set<String> value = mSharedPreferences.getStringSet(key, new HashSet<String>());
        mLogger.info("get string set key {}, value {}", key, value);
        return value;
    }

    // Clear Values Methods
    public void clearAll() {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.clear();
        mLogger.info("Clear all " + PREFERENCES_NAME);
        editor.apply();
    }

    public void clearValue(String key) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.remove(key);
        mLogger.info("clear value key {}", key);
        editor.apply();
    }


}
