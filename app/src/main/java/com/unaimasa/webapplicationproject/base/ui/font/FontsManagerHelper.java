package com.unaimasa.webapplicationproject.base.ui.font;

import android.content.Context;
import android.graphics.Typeface;

import java.util.WeakHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by unai.masa on 14/01/2016.
 * Font Manager that get custom fonts used in xml or in code
 */
public class FontsManagerHelper {

    public static final String TYPEFACE_FOLDER = "fonts";

    private static final Lock lockObject = new ReentrantLock();

    private static WeakHashMap<String, Typeface> sTypeFaces = new WeakHashMap<String, Typeface>();

    public static Typeface getTypeFace(Context context, String fileName) {
        Typeface tempTypeface = null;
        lockObject.lock();
        try {
            tempTypeface = sTypeFaces.get(fileName);

            if (tempTypeface == null) {
                String fontPath = TYPEFACE_FOLDER + "/" + fileName;
                tempTypeface = Typeface.createFromAsset(context.getAssets(), fontPath);
                sTypeFaces.put(fileName, tempTypeface);
            }
        } finally {
            lockObject.unlock();
        }


        return tempTypeface;
    }


    // If any font is added be sure change it links to right value in res/values/attrs.xml
    public enum FontName {
        NONE(""),

        PROXIMA_NOVA_SOFT_BOLD("proximanovasoft-bold.otf"),
        PROXIMA_NOVA_SOFT_MEDIUM("proximanovasoft-medium.otf"),
        PROXIMA_NOVA_SOFT_REGULAR("proximanovasoft-regular.otf"),
        PROXIMA_NOVA_SOFT_SEMIBOLD("proximanovasoft-semibold.otf");

        private String mFontFilename;

        FontName(String fontFilename) {
            this.mFontFilename = fontFilename;
        }

        public String getFontFilename() {
            return mFontFilename;
        }

        // Gget font filename from id according to xml attribute enum value
        public static FontName getFontFilenameFromId(int id) {
            switch (id) {
                // PROXIMA
                case 1:
                    return PROXIMA_NOVA_SOFT_BOLD;
                case 2:
                    return PROXIMA_NOVA_SOFT_MEDIUM;
                case 3:
                    return PROXIMA_NOVA_SOFT_REGULAR;
                case 4:
                    return PROXIMA_NOVA_SOFT_SEMIBOLD;

                default:
                    return NONE;
            }

        }
    }
}
