package com.unaimasa.webapplicationproject.base.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

import com.unaimasa.webapplicationproject.R;
import com.unaimasa.webapplicationproject.base.ui.font.FontsManagerHelper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by unai.masa on 14/01/2016.
 * Button with custom font applied
 */
public class WebAppButton extends AppCompatButton {

    private static final Logger mLogger = LoggerFactory.getLogger(WebAppButton.class);

    public WebAppButton(Context context) {
        this(context, null);
    }

    public WebAppButton(Context context, AttributeSet attrs) {
        this(context, attrs, android.R.attr.buttonStyle);
    }

    public WebAppButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        processCustomAttributes(attrs, defStyle);
    }

    private void processCustomAttributes(AttributeSet attrs, int defStyle) {
        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.WebAppButton, defStyle, 0);
        int customFontFilenameId = a.getInt(R.styleable.WebAppButton_customFont, -1);
        a.recycle();

        FontsManagerHelper.FontName fontName =
                FontsManagerHelper.FontName.getFontFilenameFromId(customFontFilenameId);
        if (fontName != FontsManagerHelper.FontName.NONE) {
            setCustomFont(fontName);
        }
    }

    public boolean setCustomFont(FontsManagerHelper.FontName fontName) {
        Typeface tf;
        try {
            tf = FontsManagerHelper.getTypeFace(getContext(), fontName.getFontFilename());
            setTypeface(tf);
        } catch (Exception e) {
            mLogger.error("Could not get typeface", e);
            return false;
        }
        return true;
    }

}
