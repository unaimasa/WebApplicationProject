package com.unaimasa.webapplicationproject.base.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.unaimasa.webapplicationproject.R;
import com.unaimasa.webapplicationproject.base.ui.font.FontsManagerHelper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by unai.masa on 14/01/2016.
 * TextView with custom font applied
 */
public class WebAppTextView extends AppCompatTextView {

    private static final Logger mLogger = LoggerFactory.getLogger(WebAppTextView.class);

    public WebAppTextView(Context context) {
        super(context);
    }

    public WebAppTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        processCustomAttributes(attrs);
    }

    public WebAppTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        processCustomAttributes(attrs);
    }

    private void processCustomAttributes(AttributeSet attrs) {
        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.WebAppTextView);
        int customFontFilenameId = a.getInt(R.styleable.WebAppTextView_customFont, -1);
        a.recycle();
        FontsManagerHelper.FontName fontName =
                FontsManagerHelper.FontName.getFontFilenameFromId(customFontFilenameId);
        if (fontName != FontsManagerHelper.FontName.NONE) {
            setCustomFont(fontName);
        }
    }

    public boolean setCustomFont(FontsManagerHelper.FontName fontName) {
        Typeface tf = null;
        try {
            tf = FontsManagerHelper.getTypeFace(getContext(), fontName.getFontFilename());
            setTypeface(tf);
        } catch (Exception e) {
            mLogger.error("Could not get typeface", e);
            return false;
        }
        return true;
    }
}
