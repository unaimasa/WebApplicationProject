package com.unaimasa.webapplicationproject.base.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import com.laimiux.rxnetwork.RxNetwork;
import com.unaimasa.webapplicationproject.Constants;
import com.unaimasa.webapplicationproject.R;
import com.unaimasa.webapplicationproject.WebApp;
import com.unaimasa.webapplicationproject.manager.ErrorHandlerHelper;
import com.unaimasa.webapplicationproject.util.CommonUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import rx.Subscription;
import rx.android.app.AppObservable;
import rx.functions.Action1;

/**
 * Created by unai.masa on 14/01/2016.
 * Base activity for all activities in app.
 * The place where general methods should be implemented.
 */
public class BaseActivity extends AppCompatActivity {

    private static final Logger mLogger = LoggerFactory.getLogger(BaseActivity.class);

    // Network
    private Subscription networkStateSubscription;
    private boolean mNetworkConnected = true;

    // 1. Activity Created
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLogger.info("Base Activity Created");

        final rx.Observable<RxNetwork.State> sendStateStream = RxNetwork.stream(this);

        networkStateSubscription = AppObservable.bindActivity(this, sendStateStream)
                .subscribe(actionNetworkState);
    }

    // 2. Activity Visible
    @Override
    protected void onStart() {
        super.onStart();
        mLogger.info("Base Activity Started");
    }

    // 3. Activity Visible
    @Override
    protected void onResume() {
        super.onResume();
        mLogger.info("Base Activity Resumed");

        // Hide the status bar.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    // 4. Activity Partially Visible
    @Override
    protected void onPause() {
        super.onPause();
        mLogger.info("Base Activity Paused");
    }

    // 5. Activity Hidden
    @Override
    protected void onStop() {
        super.onStop();
        mLogger.info("Base Activity Stopped");
    }

    // 6. Activity Destroyed
    @Override
    protected void onDestroy() {
        super.onDestroy();
        mLogger.info("Base Activity Destroyed");
        networkStateSubscription.unsubscribe();
    }

    // ----- RxAndroid Actions ----- //
    Action1<RxNetwork.State> actionNetworkState = new Action1<RxNetwork.State>(){
        @Override
        public void call(RxNetwork.State state) {
            if(state == RxNetwork.State.NOT_CONNECTED){
                CommonUtils.showToast(WebApp.getInstance().getString(R.string.error_sentence_start_template,
                        ErrorHandlerHelper.getLocalizedMessage(Constants.Errors.NETWORK_PROBLEM_ERROR)));
                mNetworkConnected = false;
            }else {
                if (!mNetworkConnected) {
                    CommonUtils.showToast(ErrorHandlerHelper.getLocalizedMessage(Constants.Errors.NETWORK_PROBLEM_SOLVED_ERROR));
                    mNetworkConnected = true;
                }
            }
        }
    };
}
