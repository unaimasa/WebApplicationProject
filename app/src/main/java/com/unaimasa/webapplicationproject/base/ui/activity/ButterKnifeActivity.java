package com.unaimasa.webapplicationproject.base.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.view.KeyEvent;
import android.view.WindowManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by unai.masa on 14/01/2016.
 */
public abstract class ButterKnifeActivity extends BaseActivity {

    private static final Logger mLogger = LoggerFactory.getLogger(ButterKnifeActivity.class);
    private final List blockedKeys = new ArrayList(
            Arrays.asList(
                    KeyEvent.KEYCODE_VOLUME_DOWN,
                    KeyEvent.KEYCODE_VOLUME_UP));

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        setContentView(getResId());
        ButterKnife.inject(this);
        afterInject(savedInstanceState);
    }

    protected void afterInject(Bundle savedInstanceState) {

    }

    @LayoutRes
    protected abstract int getResId();

    // Block Back Button
    @Override
    public void onBackPressed() {
    }

    // Block Long Power Button Press
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if(!hasFocus) {
            // Close every kind of system dialog
            Intent closeDialog = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
            sendBroadcast(closeDialog);
        }
    }

    // Disable volume button
    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (blockedKeys.contains(event.getKeyCode())) {
            return true;
        } else {
            return super.dispatchKeyEvent(event);
        }
    }

}
