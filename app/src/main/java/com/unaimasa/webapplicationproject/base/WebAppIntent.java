package com.unaimasa.webapplicationproject.base;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by unai.masa on 14/01/2016.
 * Class for storing final intent info like
 * extras keys, actions, methods for starting new activities
 */
public class WebAppIntent {

    private static final Logger mLogger = LoggerFactory.getLogger(WebAppIntent.class);

    public static void startActivity(Context context,
                                     Class<? extends AppCompatActivity> activityClazzToStart) {
        Intent intent = new Intent(context, activityClazzToStart);
        startActivity(context, intent);
    }

    public static void startActivity(Context context, Intent intent) {
        mLogger.info("Starting {}", intent);
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            try {
                context.startActivity(intent);
            } catch (ActivityNotFoundException e) {
                mLogger.error("Unable to resolve explicit activity intent");
            }
        } else {
            mLogger.error("Unable to resolve activity");
        }
    }

    public static void startActivityForResult(FragmentActivity activity, Intent intent, int requestCode) {
        activity.startActivityForResult(intent, requestCode);
    }

}
