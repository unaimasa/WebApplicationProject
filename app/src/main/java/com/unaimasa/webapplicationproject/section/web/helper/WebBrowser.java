package com.unaimasa.webapplicationproject.section.web.helper;

import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by unai.masa on 14/01/2016.
 */
public class WebBrowser extends WebViewClient {
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        view.loadUrl(url);
        return true;
    }
}
