package com.unaimasa.webapplicationproject.section.web.helper;

import android.app.Activity;
import android.webkit.JavascriptInterface;

import com.unaimasa.webapplicationproject.util.CommonUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by unai.masa on 15/01/2016.
 */
public class WebInterface {

    private static final Logger mLogger = LoggerFactory.getLogger(WebInterface.class);

    Activity mActivity;

    /** Instantiate the interface and set the context */
    public WebInterface(Activity activity) {
        mActivity = activity;
    }

    /** Show a toast from the web page */
    @JavascriptInterface
    public void showToast(String info) {
        CommonUtils.showToast(info);
    }

}
