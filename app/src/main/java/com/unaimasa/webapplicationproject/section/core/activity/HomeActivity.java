package com.unaimasa.webapplicationproject.section.core.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;

import com.unaimasa.webapplicationproject.Constants;
import com.unaimasa.webapplicationproject.R;
import com.unaimasa.webapplicationproject.WebApp;
import com.unaimasa.webapplicationproject.base.WebAppIntent;
import com.unaimasa.webapplicationproject.base.ui.activity.ButterKnifeActivity;
import com.unaimasa.webapplicationproject.base.ui.view.WebAppButton;
import com.unaimasa.webapplicationproject.base.ui.view.WebAppEditText;
import com.unaimasa.webapplicationproject.manager.ErrorHandlerHelper;
import com.unaimasa.webapplicationproject.section.web.activity.WebActivity;
import com.unaimasa.webapplicationproject.util.CommonUtils;
import com.unaimasa.webapplicationproject.util.SharedPreferencesUtil;
import com.unaimasa.webapplicationproject.util.UiUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import butterknife.InjectView;
import rx.android.view.OnClickEvent;
import rx.android.view.ViewObservable;
import rx.functions.Action1;

/**
 * Created by unai.masa on 13/01/2016.
 */
public class HomeActivity extends ButterKnifeActivity {

    private static final Logger mLogger = LoggerFactory.getLogger(HomeActivity.class);

    @InjectView(R.id.home_activity_insert_url_edit_text)
    protected WebAppEditText mUrlEditText;

    @InjectView(R.id.home_activity_go_to_web_view_button)
    protected WebAppButton mGoButton;

    @InjectView(R.id.home_activity_exit_kiosk_mode_button)
    protected WebAppButton mExitKioskModeButton;

    @Override
    protected void afterInject(Bundle savedInstanceState) {
        super.afterInject(savedInstanceState);

        // Put URL saved as default if exist in SharedPreferences
        mUrlEditText.setText(SharedPreferencesUtil.getInstance().getString(Constants.SharedKeys.WEB_VIEW_URL));

        // ----- Create View Observables for Buttons ----- //
        ViewObservable.clicks(mGoButton).subscribe(mActionGoButton);
        ViewObservable.clicks(mExitKioskModeButton).subscribe(mActionExitKioskModeButton);
    }

    @Override
    protected int getResId() {
        return R.layout.activity_home;
    }

    // ----- Action for Handling Events Rx ----- //
    Action1<OnClickEvent> mActionGoButton = new Action1<OnClickEvent>() {
        @Override
        public void call(OnClickEvent onClickEvent) {
            mLogger.info("Go Button Action Called");
            UiUtil.hideSoftKeyboard(WebApp.getInstance(), mUrlEditText);
            String newUrl = mUrlEditText.getText().toString();
            if (Patterns.WEB_URL.matcher(newUrl).matches()) {
                // Url valid
                SharedPreferencesUtil.getInstance().putString(Constants.SharedKeys.WEB_VIEW_URL, newUrl);
                CommonUtils.showToast(WebApp.getInstance().getString(R.string.home_toast_URL_updated, newUrl));
                Intent intent = new Intent(getApplicationContext(), WebActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                WebAppIntent.startActivity(getApplicationContext(), intent);
                finish();
            } else {
                // Url not valid
                CommonUtils.showToast(WebApp.getInstance().getString(R.string.error_sentence_start_template,
                        ErrorHandlerHelper.getLocalizedMessage(Constants.Errors.URL_VALIDATION_ERROR)));
            }
        }
    };

    Action1<OnClickEvent> mActionExitKioskModeButton = new Action1<OnClickEvent>() {
        @Override
        public void call(OnClickEvent onClickEvent) {
            mLogger.info("Exit Kiosk Mode Button Action Called");
            SharedPreferencesUtil.getInstance().putBool(Constants.SharedKeys.KIOSK_MODE, false);
        }
    };
}
