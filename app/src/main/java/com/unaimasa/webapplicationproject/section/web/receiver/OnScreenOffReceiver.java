package com.unaimasa.webapplicationproject.section.web.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;

import com.unaimasa.webapplicationproject.Constants;
import com.unaimasa.webapplicationproject.WebApp;
import com.unaimasa.webapplicationproject.util.SharedPreferencesUtil;

/**
 * Created by unai.masa on 18/01/2016.
 * detect a short button press by handling the ACTION_SCREEN_OFF intent
 * and kick the screen back to life with acquiring a wake lock
 */
public class OnScreenOffReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if(Intent.ACTION_SCREEN_OFF.equals(intent.getAction())){
            // is Kiosk Mode active?
            if(isKioskModeActive()) {
                wakeUpDevice();
            }
        }
    }

    private void wakeUpDevice() {
        // get WakeLock reference via WebApp
        PowerManager.WakeLock wakeLock = WebApp.get().getWakeLock();
        if (wakeLock.isHeld()) {
            wakeLock.release(); // release old wake lock
        }
        // create a new wake lock...
        wakeLock.acquire();
        // ... and release again
        wakeLock.release();
    }

    private boolean isKioskModeActive() {
        return SharedPreferencesUtil.getInstance().getBoolean(Constants.SharedKeys.KIOSK_MODE, false);
    }
}
