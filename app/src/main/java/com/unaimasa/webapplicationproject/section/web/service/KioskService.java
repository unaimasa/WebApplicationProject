package com.unaimasa.webapplicationproject.section.web.service;

import android.app.ActivityManager;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;

import com.unaimasa.webapplicationproject.Constants;
import com.unaimasa.webapplicationproject.WebApp;
import com.unaimasa.webapplicationproject.section.core.activity.HomeActivity;
import com.unaimasa.webapplicationproject.section.web.activity.WebActivity;
import com.unaimasa.webapplicationproject.util.SharedPreferencesUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by unai.masa on 18/01/2016.
 */
public class KioskService extends Service {

    private static final Logger mLogger = LoggerFactory.getLogger(KioskService.class);

    private Thread t = null;
    private Context ctx = null;
    private boolean running = false;

    @Override
    public void onDestroy() {
        mLogger.info("Stopping service 'KioskService'");
        running =false;
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mLogger.info("Starting service 'KioskService'");
        running = true;
        ctx = this;

        // start a thread that periodically checks if your app is in the foreground
        t = new Thread(new Runnable() {
            @Override
            public void run() {
                do {
                    handleKioskMode();
                    try {
                        Thread.sleep(Constants.Intervals.CHECK_INTERVAL);
                    } catch (InterruptedException e) {
                        mLogger.info("Thread interrupted: 'KioskService'");
                    }
                }while(running);
                stopSelf();
            }
        });

        t.start();
        return Service.START_NOT_STICKY;
    }

    private void handleKioskMode() {
        // is Kiosk Mode active?
        if(isKioskModeActive()) {
            // is App in background?
            if(isInBackground()) {
                // restore!
                restoreApp();
            }
        }
    }

    private boolean isInBackground() {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) WebApp.getInstance().getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(WebApp.getInstance().getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(WebApp.getInstance().getPackageName())) {
                isInBackground = false;
            }
        }
        return isInBackground;
    }

    private void restoreApp() {
        Intent i;
        // Restart activity
        if(SharedPreferencesUtil.getInstance().getBoolean(Constants.SharedKeys.WEB_VIEW_HOME)) {
            i = new Intent(ctx, HomeActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }else{
            i = new Intent(ctx, WebActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        ctx.startActivity(i);
    }

    public boolean isKioskModeActive() {
        return SharedPreferencesUtil.getInstance().getBoolean(Constants.SharedKeys.KIOSK_MODE, false);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}