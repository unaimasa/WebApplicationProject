package com.unaimasa.webapplicationproject.section.web.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.unaimasa.webapplicationproject.Constants;
import com.unaimasa.webapplicationproject.R;
import com.unaimasa.webapplicationproject.base.WebAppIntent;
import com.unaimasa.webapplicationproject.base.ui.activity.ButterKnifeActivity;
import com.unaimasa.webapplicationproject.section.core.activity.HomeActivity;
import com.unaimasa.webapplicationproject.section.web.helper.WebBrowser;
import com.unaimasa.webapplicationproject.section.web.helper.WebInterface;
import com.unaimasa.webapplicationproject.util.CommonUtils;
import com.unaimasa.webapplicationproject.util.SharedPreferencesUtil;
import com.unaimasa.webapplicationproject.util.UiUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import butterknife.InjectView;

/**
 * Created by unai.masa on 14/01/2016.
 */
public class WebActivity extends ButterKnifeActivity implements OnTouchListener{

    private static final Logger mLogger = LoggerFactory.getLogger(WebActivity.class);

    @InjectView(R.id.web_activity_view)
    protected WebView mWebView;

    protected WebSettings mWebSettings;

    private int mUnlockTouchEvents = 0;

    @Override
    protected void afterInject(Bundle savedInstanceState) {
        super.afterInject(savedInstanceState);
        SharedPreferencesUtil.getInstance().putBool(Constants.SharedKeys.WEB_VIEW_HOME, false);
        setWebView();
    }

    @Override
    protected int getResId() {
        return R.layout.activity_web;
    }

    public void setWebView(){
        mWebView.setOnTouchListener(this);

        mWebView.setWebViewClient(new WebBrowser());
        mWebView.addJavascriptInterface(new WebInterface(this), "Android");

        mWebSettings = mWebView.getSettings();
        mWebSettings.setJavaScriptEnabled(true);
        mWebSettings.setLoadsImagesAutomatically(true);

        mWebView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);

        mWebView.loadUrl(SharedPreferencesUtil.getInstance().getString(Constants.SharedKeys.WEB_VIEW_URL));
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        float touchAreaLeft = (float) 100.0;
        float touchAreaRight = UiUtil.getDeviceWidthInDp() - (float) 100.0;
        float eventAreaX = event.getX();
        float eventAreaY = event.getY();

        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:
            if ((touchAreaLeft > eventAreaX) && (touchAreaLeft > eventAreaY) && (mUnlockTouchEvents == 0)) {
                mUnlockTouchEvents++;
            } else if ((touchAreaRight < eventAreaX) && (touchAreaLeft > eventAreaY) && (mUnlockTouchEvents == 1)) {
                mUnlockTouchEvents++;
                CommonUtils.showToast("Touch Left Corner Again for Settings");
            } else if ((touchAreaLeft > eventAreaX) && (touchAreaLeft > eventAreaY) && (mUnlockTouchEvents == 2)) {
                SharedPreferencesUtil.getInstance().putBool(Constants.SharedKeys.WEB_VIEW_HOME, true);
                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                WebAppIntent.startActivity(getApplicationContext(), intent);
                finish();
            } else {
                mUnlockTouchEvents = 0;
            }
        }
        return false;
    }

}
