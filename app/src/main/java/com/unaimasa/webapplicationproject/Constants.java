package com.unaimasa.webapplicationproject;

/**
 * Created by unai.masa on 14/01/2016.
 */
public class Constants {
    public static final String EMPTY_STRING = "";
    public static final String HTTP_PATH_SEPARATOR = "/";

    public interface Errors {
        String GENERIC_ERROR = "generic_error";
        String NETWORK_PROBLEM_ERROR = "network_problem_error";
        String NETWORK_PROBLEM_SOLVED_ERROR = "network_problem_solved_error";
        String URL_VALIDATION_ERROR = "url_validation_error";
    }

    public interface SharedKeys {
        // user
        String WEB_VIEW_URL = "webapp_view_url";
        String WEB_VIEW_HOME = "webapp_home";
        // kiosk
        String KIOSK_MODE = "kiosk_mode";
    }

    public interface Intervals {
        long CHECK_INTERVAL = 100;
    }
}
