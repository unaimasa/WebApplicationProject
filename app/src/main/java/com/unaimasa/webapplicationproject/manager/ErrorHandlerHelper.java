package com.unaimasa.webapplicationproject.manager;

import com.unaimasa.webapplicationproject.Constants;
import com.unaimasa.webapplicationproject.R;
import com.unaimasa.webapplicationproject.WebApp;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by unai.masa on 14/01/2016.
 */
public class ErrorHandlerHelper {

    private static final Map<String, Integer> errorTitleMessageRegister = new HashMap<>();

    static {
        errorTitleMessageRegister.put(Constants.EMPTY_STRING, R.string.error_not_provided);
        errorTitleMessageRegister.put(Constants.Errors.NETWORK_PROBLEM_ERROR, R.string.error_network_problem);
        errorTitleMessageRegister.put(Constants.Errors.NETWORK_PROBLEM_SOLVED_ERROR, R.string.error_network_problem_solved);
        errorTitleMessageRegister.put(Constants.Errors.URL_VALIDATION_ERROR, R.string.error_url_validation);
    }

    public static String getLocalizedMessage(String errorCode) {
        int messageRes = R.string.error_not_provided;
        if (errorTitleMessageRegister.containsKey(errorCode)) {
            messageRes = errorTitleMessageRegister.get(errorCode);
        }
        return WebApp.getInstance().getString(messageRes);
    }
}
